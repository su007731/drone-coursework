package DroneCoursework;
import java.util.ArrayList;
import java.util.Arrays;

public class ConsoleCanvas {
	
	private static String[][] canvas;
	
	public ConsoleCanvas(int x, int y) {
		x = x + 2;
		y = y + 2;
		canvas = new String[(x)][y]; 
		
		// double for loop to create 2D array and populate the border with '#' and inner with spaces
		for (int i=0; i<x; i++) {
			for (int j=0; j<y; j++) {
				canvas[i][j] = " ";
			}
		}
		for (int i = 0; i < x; i++) {
		    for (int j = 0; j < y; j++) {
		        if(i==0 || j == 0 || i == x-1|| j == y-1){
		        	canvas[i][j] = "#";
		        }
		    }
		}
	}

	public static void main(String[] args) {
		ConsoleCanvas c = new ConsoleCanvas (10, 5); // create a canvas
		c.showIt(4,3,"D"); // add a Drone at 4,3
		System.out.println(c.toString()); // display result
	}
	
	//shows the drone by setting the position in the 2D array to 'D' instead of an empty space
	public void showIt(int x, int y, String d) {
		canvas[x][y] = d;
	}
	
	//creates a string to display the canvas with all the elements according to the 2D array elements
	public String toString() {
		String s = "";
		for (String[] x : canvas){
		   for (String y : x) {
		        s += y;
		   }
		   s += "\n";
		}
		return s;
	}

}
