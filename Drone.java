package DroneCoursework;
import java.io.Serializable;

public class Drone implements Serializable  {
	private static int counter = 0;
	
	private int x;
	private int y;
	private int id;
	private Direction d;
	
	//constructor for creating the drone
	public Drone(int i, int j, Direction dir) {
		x = i;
		y = j;
		id = counter++;
		d = dir;
	}

	public static void main(String[] args) {
		Drone d = new Drone(5, 3, Direction.North);	
		Drone dd = new Drone(6, 2, Direction.North);
		System.out.println(dd.toString());	// print where is
		
	}
	
	public void displayDrone(ConsoleCanvas c) {
//		call the showIt method in c to put a D where the drone is
		c.showIt(x, y, "d");
	}
	
	public void tryToMove(DroneArena arena) {
		tryToMove(arena, 0);
	}
	
	//attempts to move a single drone
	private void tryToMove(DroneArena arena, int counter) {
		//counter is used to attempt to move in all 4 directions before giving up
		if (counter == 3) {
			d = Direction.getNextDirection(d);
			return;
		}
		int nextX = x;
		int nextY = y;
		switch (d) {
        case North:
            nextY += 1;
            break;
        case East:
        	nextX += 1;
            break;
		case South:
			nextY -= 1;
            break;
		default:
			nextX -= 1;
            break;
		} 
		
		//if the drone can't move to the next x and y, it's called recursively 
		//to try again in the next direction
		if (!arena.canMoveHere(nextX, nextY)) {
			d = Direction.getNextDirection(d);
			tryToMove(arena, counter+=1);
			return;
		}
		x = nextX;
		y = nextY;
	}

	public String toString() {
		return "ID: " + id + "\nX coordinate: " + x + "\nY Coordinate: " + y
				+ "\nDirection: " + d;
	}
	
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

}
