package DroneCoursework;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

public class DroneArena implements Serializable {

	private ArrayList<Drone> drones;
	private int height;
	private int width;
	private Random r;
	
	public DroneArena(int height, int width) {
		drones = new ArrayList<Drone>();
		r = new Random();
		this.height = height;
		this.width = width;
	}

	//adds a drone randomly using r and checks if the position in the drones array is free before adding it
	public void addDrone() {
		int x = r.nextInt(width);
		int y = r.nextInt(height);
		while (isDroneTaken(x, y)) {
			x = r.nextInt(width);
			y = r.nextInt(height);
		}
		if (canMoveHere(x, y)) {
			Drone d = new Drone(x,y, Direction.getRandomDirection());
			drones.add(d);
		} else {
			addDrone();
		}
	}
	
	public static void main(String[] args) {
		DroneArena a = new DroneArena(20, 10);	// create drone arena
		a.addDrone();
		a.addDrone();
		System.out.println(a.toString());	// print where is
		a.moveAllDrones();
	}
	
	//creates a string of the arenas size and all the drones information inside the arena
	public String toString() {
		String s = "Arena Size: " + height + "x" + width + "\n";
		for (Drone d : drones) { 
		    s += d.toString()+"\n";
		}
		return s;
	}
	
	//gets the drones matching the x and y parameters in the arraylist
	public Drone getDroneAt(int x, int y) {
		for (Drone d : drones) { 
		    if (d.getX() == x && d.getY() == y) {
		    	return d;
		    } 
		}
		return null;
  }
	
	//checks if the drone is taken by using the getDroneAt method
	public boolean isDroneTaken(int x, int y) {
		return getDroneAt(x,y) != null;
	}
	
	//checks if the drone is within the arena to see if it's able to move to the x and y
	public boolean canMoveHere(int x, int y) {
		if (x < 1 || x > width || y < 1 || y > height) {
			return false;
		}
		return !isDroneTaken(x, y);
	}
	
	//loops through every drone in the drone arraylist and moves them
	public void moveAllDrones() {
		for (Drone d : drones) { 
			d.tryToMove(this);
		}
	}
	
	//loops through every drone in the drone arraylist and displays them in the console
	public void showDrones(ConsoleCanvas c) {
//		loop through all the Drones calling the displayDrone method >>
		for (Drone d : drones) { 
		    d.displayDrone(c);
		}
	}

	
	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}


}
