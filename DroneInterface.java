package DroneCoursework;

import java.io.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.Serializable;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileSystemView;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

/**
 * Simple program to show arena with multiple drones
* @author shsmchlr
 *
 */
public class DroneInterface implements Serializable {
	
	private Scanner s;								// scanner used for input from user
    private DroneArena myArena;				// arena in which drones are shown
    /**
    	 * constructor for DroneInterface
    	 * sets up scanner used for input and the arena
    	 * then has main loop allowing user to enter commands
     * @throws InterruptedException 
     */
    public DroneInterface() throws InterruptedException {
    	 s = new Scanner(System.in);			// set up scanner for user input
    	 myArena = new DroneArena(20, 6);	// create arena of size 20*6
    	
        char ch = ' ';
        do {
        	System.out.print("Enter (A)dd drone, get (I)nformation, "
         			+ "get (D)isplay, (M)ove all drones,\n (F) Animate Drones, "
         			+ "(C)reate a new arena,\n (S)ave an arena, (L)oad an arena" 
         			+ " or e(X)it > ");
        	ch = s.next().charAt(0);
        	s.nextLine();
        	switch (ch) {
    			case 'A' :
    			case 'a' :
        					myArena.addDrone();	// add a new drone to arena
        					break;
        		case 'I' :
        		case 'i' :
        					System.out.print(myArena.toString());
            				break;
        		case 'D' :
        		case 'd' :
        					doDisplay(myArena);
            				break;
        		case 'M' :
        		case 'm' :
        					moveAllDrones();
            				break;
        		case 'F' :
        		case 'f' :
        					animate();
            				break;
        		case 'C' :
        		case 'c' :
        					createNewArena();
            				break;
        		case 'S' :
        		case 's' :
        					saveArena();
            				break;
        		case 'L' :
        		case 'l' :
        					loadArena();
            				break;
        		case 'x' : 	ch = 'X';				// when X detected program ends
        					break;
        	}
    		} while (ch != 'X');						// test if end
        
       s.close();									// close scanner
    }
    
    private void doDisplay(DroneArena arena) {
		// determine the arena size 
		//hence create a suitable sized ConsoleCanvas object
		//call showDrones suitably
		//then use the ConsoleCanvas.toString method 
    	ConsoleCanvas c = new ConsoleCanvas(arena.getWidth(), arena.getHeight());
    	arena.showDrones(c);
    	System.out.println(c.toString());
	}
    
    //moves all the drones on the arena
    private void moveAllDrones() {
    	myArena.moveAllDrones();
    	System.out.print(myArena.toString());
    	doDisplay(myArena);
    }
    
    //creates a new arena and replaces the current one with the specs of the new one
    private void createNewArena() {
    	Scanner scan = new Scanner(System.in);  // Create a Scanner object
        System.out.println("Enter X Length");
        String xlength = scan.nextLine();  // Read user input
        System.out.println("Enter Y Length");
        String ylength = scan.nextLine();  // Read user input
        myArena = new DroneArena(Integer.parseInt(ylength), Integer.parseInt(xlength));
        doDisplay(myArena);
    }
    
    //calls to move all drones 20 times every 200ms
    private void animate(){
    	for (int i=0; i<20; i++) {
    		moveAllDrones();
    		try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    }
    
    //saves the arena object in the 'arena.txt' file using serialization
    public void saveArena(){
    	try {
            FileOutputStream fileOut = new FileOutputStream("arena.txt");
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(myArena);
            objectOut.close();
            System.out.println("The Object  was succesfully written to a file");
 
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    //loads the arena object in the 'arena.txt' file using serialization
    private void loadArena() {
    	try{
    	    FileInputStream readData = new FileInputStream("arena.txt");
    	    ObjectInputStream readStream = new ObjectInputStream(readData);

    	    myArena = (DroneArena) readStream.readObject();
    	    System.out.println("Loaded arena");
    	    readStream.close();
    	}catch (Exception e) {
    	    e.printStackTrace();
    	}
    }
    
	public static void main(String[] args){
		try {
			DroneInterface r = new DroneInterface();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	// just call the interface
	}

}