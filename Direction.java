package DroneCoursework;
import java.util.Random;

public enum Direction {
	North,
	East,
	South, 
	West;
	
	//returns a random element from the Direction enum
	public static Direction getRandomDirection() {
        Random random = new Random();
        return values()[random.nextInt(values().length)];
    }
	
	//gets the next direction clockwise on the compass to it's current direction
	public static Direction getNextDirection(Direction dir) {
		switch (dir) {
        case North:
            return East;
        case East:
        	return South;
		case South:
        	return West;
		default:
			return North;
		}
    }
}
